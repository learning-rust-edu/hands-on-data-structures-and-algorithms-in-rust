pub mod error;
pub mod blob;
pub mod blobstore;

#[cfg(test)]
mod tests {
    #[test]
    fn test_it() {
        assert_eq!(2 + 2, 4);
    }
}
