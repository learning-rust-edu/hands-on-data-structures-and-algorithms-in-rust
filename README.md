# Hands-On Data Structures and Algorithms in Rust

Sample code for "Hands-On Data Structures and Algorithms in Rust" video course from Packt Publishing.

## License
[MIT](./LICENSE)
