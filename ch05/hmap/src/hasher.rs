use std::hash::{Hash, Hasher};

pub struct HMap {
    prev: u8,
    n: u128,
}

impl Hasher for HMap {
    fn finish(&self) -> u64 {
        self.n as u64
    }

    fn write(&mut self, bytes: &[u8]) {
        for d in bytes {
            self.n = ((self.n + 11) * (*d as u128 + 13) + ((d ^ self.prev) as u128))
                % (u64::MAX as u128);
            self.prev = *d;
        }
    }
}

pub fn hash<T: Hash>(seed: u64, t: T) -> u64 {
    let mut h = HMap { n: 0, prev: 0 };
    h.write_u64(seed);
    t.hash(& mut h);
    h.finish()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hasher() {
        let n = hash(55, "cat");

        assert_eq!(n, hash(55, "cat"));

        assert_ne!(hash(55, "abc"), hash(55, "cba"));
    }

    #[test]
    fn test_numbers() {
        let mut prev = 0;
        for x in 0..10_000 {
            let curr = hash(55, x);
            assert_ne!(curr, prev);
            prev = curr;
        }
    }
}
