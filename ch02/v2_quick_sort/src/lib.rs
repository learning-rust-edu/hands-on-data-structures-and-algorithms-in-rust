mod b_rand;

use std::fmt::Debug;
use std::thread;

// Move first element to the correct place
// Everything lower should be before it,
// everything higher should be after it
// return it's location
pub fn pivot<T: PartialOrd>(v: &mut [T]) -> usize {
    let mut p = 0;
    for i in 1..v.len() {
        if v[i] < v[p] {
            // move our pivot forward 1, and put this element before it
            v.swap(p + 1, i);
            v.swap(p, p + 1);
            p += 1
        }
    }
    p
}

pub fn pivot_improved<T: PartialOrd>(v: &mut [T]) -> usize {
    let p = b_rand::rand(v.len());
    v.swap(p, 0);
    pivot(v)
}

pub fn quick_sort<T: PartialOrd + Debug>(v: &mut [T]) {
    if v.len() <= 1 {
        return;
    }
    let p = pivot(v);
    println!("{:?}", v);

    let (a, b) = v.split_at_mut(p);
    quick_sort(a);
    quick_sort(&mut b[1..]); // Middle element already sorted
}

pub fn quick_sort_improved<T: PartialOrd + Debug>(v: &mut [T]) {
    if v.len() <= 1 {
        return;
    }
    let p = pivot_improved(v);
    println!("{:?}", v);

    let (a, b) = v.split_at_mut(p);
    quick_sort(a);
    quick_sort(&mut b[1..]); // Middle element already sorted
}

pub fn quick_sort_threaded<T: 'static + PartialOrd + Debug + Send>(v: &mut [T]) {
    if v.len() <= 1 {
        return;
    }
    let p = pivot_improved(v);
    println!("{:?}", v);

    let (a, b) = v.split_at_mut(p);

    // NOTE: this will spawn infinite amount of threads
    thread::scope(|s| {
        let handle = s.spawn(move || {
            quick_sort_threaded(a);
        });
        quick_sort_threaded(&mut b[1..]);
        handle.join().ok();
    });
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pivot() {
        let mut v = vec![4, 6, 1, 19, 8, 11, 12, 3];

        let p = pivot(&mut v);

        for x in 0..v.len() {
            assert_eq!(v[x] < v[p], x < p);
        }
    }

    #[test]
    fn test_pivot_improved() {
        let mut v = vec![4, 6, 1, 19, 8, 11, 12, 3];

        let p = pivot_improved(&mut v);

        for x in 0..v.len() {
            assert_eq!(v[x] < v[p], x < p);
        }
    }

    #[test]
    fn test_quick_sort() {
        let mut v = vec![4, 6, 1, 19, 8, 11, 12, 3];

        quick_sort(&mut v);

        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 12, 19]);
    }

    #[test]
    fn test_quick_sort_sorted() {
        // quick sort will go through sorted array and complexity will be O(n^2)
        let mut v = vec![1, 3, 4, 6, 8, 11, 12, 19];

        quick_sort(&mut v);

        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 12, 19]);
    }

    #[test]
    fn test_quick_sort_improved() {
        let mut v = vec![4, 6, 1, 19, 8, 11, 12, 3];

        quick_sort_improved(&mut v);

        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 12, 19]);
    }

    #[test]
    fn test_quick_sort_improved_sorted() {
        // with random number complexity is improved
        let mut v = vec![1, 3, 4, 6, 8, 11, 12, 19];

        quick_sort_improved(&mut v);

        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 12, 19]);
    }

    #[test]
    fn test_quick_sort_threaded() {
        let mut v = vec![4, 6, 1, 19, 8, 11, 12, 3];

        quick_sort_threaded(&mut v);

        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 12, 19]);
    }
}
