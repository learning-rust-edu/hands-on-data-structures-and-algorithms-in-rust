fn main() {
    for i in 0..10 {
        println!("fib({}), naive = {}, iter = {}", i, fibonacci(i), fibonacci_iter(i));
        println!("dynamic = {}", fibonacci_dynamic(i).0);
        println!("tail recursive = {}", fibonacci_dynamic_tail_recursive(i, None, None));
    }
}

// fib 5 = fib 4 + fib 3
// = fib 3 + fib 2 + fib 2 + fib 1
// = fib 2 + fib 1 + fib 1 + fib 0 + fib 1 + fib 0 + fib 1 = 8
pub fn fibonacci(n: i32) -> i32 {
    if n <= 0 {
        return 0;
    }
    if n <= 1 {
        return 1;
    }
    // fibonacci(n - 1) will call fibonacci(n - 2) so we do that function twice
    fibonacci(n - 1) + fibonacci(n - 2)
}

pub fn fibonacci_iter(n: i32) -> i32 {
    if n <= 0 {
        return 0;
    }
    let mut a = 0;
    let mut b = 1;
    let mut res = 1;

    for _ in 1..n {
        res = a + b;
        a = b;
        b = res;
    }
    res
}

// return (res, prev)
// if function is going to be used more than once, need to store result somewhere
pub fn fibonacci_dynamic(n: i32) -> (i32, i32) {
    if n <= 0 {
        return (0, 0);
    }
    if n == 1 {
        return (1, 0);
    }
    let (a, b) = fibonacci_dynamic(n - 1);
    (a + b, a)
}

// defaults: a = 0, b = 1
pub fn fibonacci_dynamic_tail_recursive(n: i32, a: Option<i32>, b: Option<i32>) -> i32 {
    let a = a.unwrap_or(0);
    let b = b.unwrap_or(1);
    if n <= 0 {
        return a;
    }
    if n == 1 {
        return b;
    }
    fibonacci_dynamic_tail_recursive(n - 1, Some(b), Some(a + b))
}
